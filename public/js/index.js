var currentTab;

window.onload = function (event) {
    currentTab = checkStep();
    getStoredValues();
    showTab(currentTab); // Display the current tab
}

function checkStep() {
    if (localStorage.getItem('step')) {
        return Number(localStorage.getItem('step'));
    } else {
        return 0;
    }
}

function getStoredValues() {
    var i, inputs, inputId;
    inputs = document.getElementsByTagName('input')
    for (i=0; i<inputs.length; i++) {
        inputId = inputs[i].getAttribute('id');
        if (localStorage.getItem(inputId)) {
            inputs[i].value = localStorage.getItem(inputId);
        }
    }
}

function showTab(currentTab) {
    var tabs = document.getElementsByClassName("tab");
    tabs[currentTab].style.display = "block";
    setPrevNextButtons(tabs.length, currentTab);
}

function setPrevNextButtons(tabLength, currentTab) {
    if (currentTab == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (currentTab == (tabLength - 1)) {
        document.getElementById("nextBtn").innerHTML = "Submit";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
}

function nextPrev(n) {
    var tabs = document.getElementsByClassName("tab");

    if (n == 1) {
        storeCurrentTabValues(tabs[currentTab]);
        if (!validateForm()) return false;
    }

    tabs[currentTab].style.display = "none";
    currentTab = currentTab + n;

    if (n == 1) {
        localStorage.setItem('step', currentTab);
    }

    if (currentTab >= tabs.length) {
        localStorage.clear();
        document.getElementById("regForm").submit();
        return false;
    }

    showTab(currentTab);
}

function validateForm() {
    var tabs, inputs, i, valid = true;
    tabs = document.getElementsByClassName("tab");
    inputs = tabs[currentTab].getElementsByTagName("input");
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i].value == "") {
            inputs[i].className += " invalid";
            valid = false;
        }
    }
    return valid;
}

function storeCurrentTabValues(currentTab) {
    var elementId, elementValue;
    var inputs = currentTab.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
        elementId = inputs[i].getAttribute('id');
        elementValue = inputs[i].value;
        localStorage.setItem(elementId, elementValue);
    }
}