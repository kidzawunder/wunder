# Wunderfleet App

The web application is a basic user registration, like typically known from similar
projects. The registration process contains 4 separated steps. Only one step is shown at a time
to the customer

### Prerequisites

```
PHP > 7.0
Database server (MySQL, MariaDB) 
```

### Installing

1. Install packages
    ```
    composer install 
    ```
2. change db params (db_user, db_password, db_name) in file .env
    ```
    DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.6"
    ```
3. start db migration command 
    ```
    php bin\console doctrine:migrations:migrate
    ```
4.  start local symfony server 
    ```
    symfony server:start
    ```

### Performance optimizations

* submiting form through AJAX call 
* async API call 

### Which things could be done better

* tests
* normalization of users data (separating users's payment info - writting in the separated table, it depends on project requirements)
* Docker

## Built With

* [Symfony](https://symfony.com/) - The web framework used

