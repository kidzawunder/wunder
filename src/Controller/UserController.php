<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserFormType;
use App\PaymentDataHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="registerUser")
     */
    public function index(Request $request, EntityManagerInterface $entityManager,
                          PaymentDataHandler $paymentDataHandler): Response
    {
        $user = new User();
        $form = $this->createForm(UserFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($user);
            $entityManager->flush();

            $paymentDataId = $paymentDataHandler->getPaymentId($user);

            if ($paymentDataId != null) {
                $user->setPaymentDataId($paymentDataId);
                $entityManager->persist($user);
                $entityManager->flush();
            }

            return $this->render('user/success.html.twig', [
                'paymentDataId' => $paymentDataId
            ]);
        }

        return $this->render('user/index.html.twig', [
            'userForm' => $form->createView()
        ]);
    }
}
