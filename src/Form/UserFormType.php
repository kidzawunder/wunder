<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('telephone')
            ->add('street')
            ->add('house_number')
            ->add('zip')
            ->add('city')
            ->add('owner')
            ->add('iban')
//            ->add('submit',  SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
//            'csrf_protection' => true,
//            'csrf_field_name' => '_token',
//            'csrf_token_id'   => 'user_registration_token',
        ]);
    }
}
