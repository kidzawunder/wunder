<?php
/**
 * Created by PhpStorm.
 * User: zorica
 * Date: 2/20/2021
 * Time: 2:29 PM
 */

namespace App;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PaymentDataHandler
{
    private $client;
    private $endpoint;

    public function __construct(HttpClientInterface $client, string $paymentUrl)
    {
        $this->client = $client;
        $this->endpoint = $paymentUrl;
    }

    public function getPaymentId($user): ?string
    {
        try {
            $response = $this->client->request('POST', $this->endpoint,
                [
                'body' =>  json_encode([
                    'customerId'    => $user->getId(),
                    'iban'          => $user->getIban(),
                    'owner'         => $user->getOwner()
                ]),
            ]);
            $content = $response->toArray();
            return $content['paymentDataId'];
        } catch (\Exception $e) {
            //log error
            return null;
        }
    }
}